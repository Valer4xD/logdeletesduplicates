﻿// LogDeletesDuplicates.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.


#include "stdafx.h"

using namespace std;

enum class side
{
    CLIENT  = 0,
    SERVICE = 1
};

void list_dir        (const string & path, side _side, int level);
void process_file    (string dir, string file_name, side _side);
bool is_equal_reverse(string str1, string str2);
int  is_equal        (string str1, string str2);

int _files_total = 0,
    _records_old_total = 0,
    _records_new_total = 0;

string project_directory_without_logging;
int length_of_the_project_directory_with_logging;

string dir_wo_repeats_client;
string dir_wo_repeats_service;

bool dir_exists(const string & dir_name)
{
    DWORD ftyp = GetFileAttributesA(dir_name.c_str());

    if (ftyp == INVALID_FILE_ATTRIBUTES)
        return false;

    return ftyp & FILE_ATTRIBUTE_DIRECTORY;
}

int main(int argc, char* argv[])
{
    if(argc < 5)
    {
        cout
            << "\tError!" << endl
            << "\t\tCommand line arguments required:" << endl
            << "\t\t\tClient process name;" << endl
            << "\t\t\tService process name;" << endl
            << "\t\t\tProject directory without logging;" << endl
            << "\t\t\tLength of the project directory with logging." << endl
            << "\t\tCreate shortcut, right click, properties, object, add." << endl;
        return 1;
    }

    string dir_main = "C:/DEBUG/";
    string dir_wo_repeats = dir_main + "wo_repeats/";
    dir_wo_repeats_client = dir_wo_repeats + "client/";
    dir_wo_repeats_service = dir_wo_repeats + "service/";

    if( ! dir_exists(dir_main))                 _mkdir(dir_main.c_str());
    if( ! dir_exists(dir_wo_repeats))           _mkdir(dir_wo_repeats.c_str());
    if( ! dir_exists(dir_wo_repeats_client))    _mkdir(dir_wo_repeats_client.c_str());
    if( ! dir_exists(dir_wo_repeats_service))   _mkdir(dir_wo_repeats_service.c_str());

    string dir_original_client = dir_main + argv[1];
    string dir_original_service = dir_main + argv[2];
    project_directory_without_logging = argv[3];
    length_of_the_project_directory_with_logging = atoi(argv[4]);

    replace(dir_original_client.begin(), dir_original_client.end(), '\\', '/');
    replace(dir_original_service.begin(), dir_original_service.end(), '\\', '/');
    replace(project_directory_without_logging.begin(), project_directory_without_logging.end(), '/', '\\');

    time_t begin = time(NULL);
    list_dir(dir_original_client, side::CLIENT, 0);
    list_dir(dir_original_service, side::SERVICE, 0);
    time_t end = time(NULL);

    cout
        << endl
        << "Time spent: ["
        << to_string(end - begin)
        << "] (seconds)"
        << endl;

    system("pause");

    return 0;
}

void list_dir(const string & dir, side _side, int level)
{
    DIR    * dir_ptr = opendir(dir.c_str());
    dirent * entry;
    string   file_name = "";
    int      inserts = 0 ;

    if(!dir_ptr)
    {
        perror("opendir");
        system("pause");
        exit  (1);
    };

    while((entry = readdir(dir_ptr)))
    {
        file_name = entry->d_name;

        if(file_name != "."
        && file_name != "..")
            switch(entry -> d_type)
            {
                case DT_DIR: // directories
                {
                    list_dir(dir + "/" + file_name, _side, level + 1);

                    break;
                }

                case DT_REG: // files
                {
                    _files_total++;

                    process_file(dir, file_name, _side);

                    break;
                }
            }
    }

    closedir(dir_ptr);
}

void process_file(string dir, string file_name, side _side)
{
    const int MAX_LINES = 4;
    enum log_struct_old
    {
        TIME = 0,
        PATH = 1,
        LINE_ORIGINAL = 2,
        LINE_WITH_LOG = 3
    };

    int line = 0;
    bool repeat;
    string log_method_old_temp[MAX_LINES];
    log_struct log_method_new_temp = log_struct();
    vector<log_struct> logs_new;
                       logs_new.reserve(1000);
    int len_path_with_log,
        c;
    string temp_path_original,
           temp_path_with_log;

    int last_id,
        v;

    ifstream fin(dir + "/" + file_name, ios::in);
    fin.seekg(3);
    while(getline(fin, log_method_old_temp[line ++]))
    {
        if(MAX_LINES <= line)
        {
            line = 0;

            log_method_new_temp.time_cur      = atoll(log_method_old_temp[TIME         ].c_str());

            temp_path_with_log = log_method_old_temp[PATH];
             len_path_with_log = temp_path_with_log.length();

             temp_path_original = project_directory_without_logging;

             for(c = length_of_the_project_directory_with_logging; c < len_path_with_log; ++ c)
                temp_path_original.push_back(temp_path_with_log[c] == '/' ? '\\' : temp_path_with_log[c]);

            log_method_new_temp.path_original = temp_path_original;

            log_method_new_temp.path_with_log =      log_method_old_temp[PATH         ];
            log_method_new_temp.line_original = atoi(log_method_old_temp[LINE_ORIGINAL].c_str());
            log_method_new_temp.line_with_log = atoi(log_method_old_temp[LINE_WITH_LOG].c_str());

            repeat = false;

            last_id = logs_new.size() - 1;
            for(v = last_id; v >= 0; -- v)
                if(log_method_new_temp.line_original == logs_new[v].line_original)
                if(log_method_new_temp.line_with_log == logs_new[v].line_with_log)
                    if(is_equal_reverse(log_method_new_temp.path_with_log, logs_new[v].path_with_log))
                    {
                        if(logs_new[v].time_first > log_method_new_temp.time_cur)       logs_new[v].time_first = log_method_new_temp.time_cur;
                        if(logs_new[v].time_last  < log_method_new_temp.time_cur)       logs_new[v].time_last  = log_method_new_temp.time_cur;

                        logs_new[v].count ++;

                        repeat = true;
                        break;
                    }

            ++ _records_old_total;

            if(!repeat)
            {
                log_method_new_temp.time_first = log_method_new_temp.time_cur;
                log_method_new_temp.time_last  = log_method_new_temp.time_cur;
                log_method_new_temp.count = 1;

                logs_new.push_back(log_method_new_temp);

                _records_new_total ++;

                cout << _records_old_total << "\t" << _records_new_total << endl; // Прогресс.
            }
        }
    }
    fin.close();

    last_id = logs_new.size() - 1;
    int equal = 0;
    log_struct temp;
    bool  swap = true;
    while(swap) {
          swap = false;
        for(v = 0; v < last_id; ++ v)
        {
            equal = is_equal(logs_new[v].path_with_log, logs_new[v + 1].path_with_log);

            if(equal > 0
            ||!equal && logs_new[v].line_original > logs_new[v + 1].line_original)
            {
                temp            = logs_new[v];
                logs_new[v]     = logs_new[v + 1];
                logs_new[v + 1] = temp;
                swap = true;
            }
        }
    }

    /*
    int c;
    int len;
    for(v = 0; v <= last_id; ++ v)
    {
        len = logs_new[v].path_with_log.length();
        for(c = 0; c < len; ++ c)           if(logs_new[v].path_with_log[c] == '/')     logs_new[v].path_with_log[c] = '\\';
    }
    */

    string path_result = _side == side::CLIENT ? dir_wo_repeats_client  + file_name
                                               : dir_wo_repeats_service + file_name;
    ofstream fout(path_result, ios::out);
    if(!fout.is_open())
    {
        perror("ofstream.is_open");
        system("pause");
        exit(1);
    }
    int size_new = logs_new.size();
    log_struct cur_cache;
    for(int r = 0 ; r < size_new; ++ r)
    {
        cur_cache = logs_new[r];
        fout << cur_cache.time_first    << (cur_cache.time_first    < 1000 ? "\t\t-   " : "\t-   ")
             << cur_cache.time_last     << (cur_cache.time_last     < 1000 ? "\t\t    " : "\t    ")
             << cur_cache.count         << (cur_cache.count         < 1000 ? "\t\t    " : "\t    ")
             << cur_cache.line_original << (cur_cache.line_original < 1000 ? "\t\t-   " : "\t-   ")
             << cur_cache.line_with_log << (cur_cache.line_with_log < 1000 ? "\t\t    " : "\t    ")
             << cur_cache.path_original << endl;
    }
    fout.close();

    vector<log_struct>(logs_new).swap(logs_new);
}

bool is_equal_reverse(string str1, string str2)
{
    int start_str1 = str1.length() - 1;
    int start_str2 = str2.length() - 1;

    if(start_str1 != start_str2)    return false;

    for(;
               (start_str1 >= 0)
            && (start_str2 >= 0);
                -- start_str1,
                -- start_str2)      if(str1[start_str1] != str2[start_str2])    return false;

    return true;
}

int is_equal(string str1, string str2)
{
    int start_str1 = 0,
        start_str2 = 0,
        len_str1 = str1.length(),
        len_str2 = str2.length();

    for(;
               (start_str1 < len_str1)
            && (start_str2 < len_str2);
                ++ start_str1,
                ++ start_str2)      if(str1[start_str1] != str2[start_str2])    return str1[start_str1] - str2[start_str2];

    if(len_str1 == len_str2)        return 0;
    else
    if(len_str1 >  len_str2)        return  str1[start_str1];
    else                            return -str1[start_str2];

    return 0;
}

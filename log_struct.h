#include "stdafx.h"

using namespace std;

class log_struct
{
	public: long long time_cur   = 0,
			  time_first = 0,
			  time_last  = 0;

	public: int count = 0;

	public: int line_original = 0,
				line_with_log = 0;

	public: string path_original = "",
				   path_with_log = "";
};

